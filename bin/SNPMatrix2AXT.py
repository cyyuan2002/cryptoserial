#!/usr/bin/env python
LIBPATH = "/dscrhome/yc136/bin/Python/Library"

import os
import csv
import sys
os.sys.path.append(LIBPATH)
import GFFParser
import GenomeParser
import copy
from Bio import SeqIO

NonChar = '.'

def loadFasta(gfile):
    fh = open(gfile,'r')
    genome = {}
    for seq in SeqIO.parse(fh,"fasta"):
        genome[seq.id] = seq.seq.tomutable()
    fh.close()
    return genome

def immuGenome(genome):
    for chrom in genome:
        genome[chrom] = genome[chrom].toseq()
    return genome

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write ("Usage:%s <SNP_matrix> <Ref_genome> <Ref_GFF>\n" %(sys.argv[0]))
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    fgenome = sys.argv[2]
    fgff = sys.argv[3]
    
    fout = fmatrix + ".axt"
    
    genome = loadFasta(fgenome)
    gffparser = GFFParser.GFFParser(fgff)
    
    fh_matrix = open(fmatrix, 'rU')
    reader = csv.reader(fh_matrix, delimiter="\t")
    header = reader.next()
    
    genomeA = copy.deepcopy(genome)
    genomeB = copy.deepcopy(genome)
    
    vargenes = []
    
    for row in reader:
        if len(row) < 1:
            continue
        snptype, gene = gffparser.siteType(row[0], row[1])
        #print ("\t%s\t%s\t%s\t%s" %(row[0], row[1], snptype, gene))
        variants = []
        variants.append(row[2])
        variants.extend(row[3].split(','))
        snpA = variants[int(row[4])]
        snpB = variants[int(row[5])]
        if snptype == "CDS":
            genomeA[row[0]][int(row[1])-1] = snpA
            genomeB[row[0]][int(row[1])-1] = snpB
            if gene not in vargenes:
                vargenes.append(gene)
    
    fh_matrix.close()
    
    ##bug in mutable genome for reverse_complement
    genomeA = immuGenome(genomeA)
    genomeB = immuGenome(genomeB)
    
    gparserA = GenomeParser.GenomeParser(genomeA,gffparser)
    gparserB = GenomeParser.GenomeParser(genomeB,gffparser)
    
    for gene in vargenes:
        cdsA = gparserA.getCDSSeq(gene)
        cdsB = gparserB.getCDSSeq(gene)
        print (gene)
        print(cdsA)
        print(cdsB)
        print("")
