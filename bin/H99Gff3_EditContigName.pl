#!/usr/bin/perl
use strict;

my $file =shift;
open(my $fh_file, "$file");
while (<$fh_file>) {
    chomp();
    if ($_ eq "") {
        print "\n";
    }
    else{
        my @lines =split('\t',$_);
        $lines[0] = $lines[0]."_of_Cryptococcus_neoformans_grubii_H99";
        print join("\t",@lines),"\n";
    }
}
close $fh_file;
