#!/usr/bin/perl -w
use strict;
my $filein;
my $fileout;
my $pattern;

($filein, $fileout,$pattern) = @ARGV;

my $fh_file;
my $fh_out;
open($fh_file, "$filein") or die "Cannot open file $filein";
open($fh_out, ">$fileout");
my $isfirst = 1;
while (<$fh_file>) {
    if (/##fileformat=VCFv4.1/) {
        if ($isfirst) {
            $isfirst = 0;
            print $fh_out $_;
            next;
        }
        else{
            last;
        }
    }
    if (/^#/) {
        print $fh_out $_;
    }
    else{
        my @lines =split(/\t/,$_);
        if (length($lines[3]) > 1 || length($lines[4]) > 1) {
            last;
        }
        
        if ($lines[6] eq $pattern) {
            print $fh_out $_;
        }
    }
}

close($fh_file);
close($fh_out);
exit(0);
