#!/usr/bin/env perl
##This script is used to seperate DEL/DUP from CNVnator vcf results 

use strict;

my ($file,$type) =@ARGV;

open(my $fh_filein,"$file");
while(<$fh_filein>){
	if(/^#/){
		print $_;
	}
	else{
		my @lines = split(/\t/,$_);
		if($lines[4] =~ /$type/){
			print $_;
		}
	}
}
close $fh_filein;

exit(0);