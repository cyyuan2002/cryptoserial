#!/usr/bin/env python
import sys
import csv

geneinfo = {}
caseinfo = []

for i in xrange(1, len(sys.argv)):
    fileN = sys.argv[i].split('.')
    caseID = 'case_' + fileN[1] + '_' + fileN[2]
    caseinfo.append(caseID)
    fh = open(sys.argv[i], 'r')
    reader = csv.reader(fh, delimiter = '\t')
    reader.next()
    for line in reader:
        kaks = {}
        if line[4] == 'NA':
            continue;
        kaks['omega'] = float(line[4])
        kaks['pvalue'] = float(line[5])
        kaks['sub'] = float(line[10])
        kaks['ds'] = float(line[11])
        kaks['dn'] = float(line[12])
        if(line[0] not in geneinfo):
            geneinfo[line[0]] = {}
            geneinfo[line[0]][caseID] = kaks
        else:
            geneinfo[line[0]][caseID] = kaks

sys.stdout.write("#GeneID")
for case in caseinfo:
    sys.stdout.write("\t"+case)
sys.stdout.write("\n")

for gene in geneinfo:
    sys.stdout.write(gene)
    kaksinfo = geneinfo[gene]
    for case in caseinfo:
        if(case not in kaksinfo):
            sys.stdout.write('\tNA')
        else:
            sys.stdout.write('\t%.4f(%.4f)' %(kaksinfo[case]['omega'], kaksinfo[case]['pvalue']))
    sys.stdout.write('\n')
