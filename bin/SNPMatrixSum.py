#!/usr/bin/env python
import sys
import csv

infile = sys.argv[1]
fh_file = open(infile,'rU')
reader = csv.reader(fh_file,delimiter="\t")
reader.next()

for line in reader:
    values = map(int,line[2:len(line)])
    sumvalue = sum(values)
    print ("%s\t%s\t%s" %(line[0],line[1],sumvalue))

fh_file.close()