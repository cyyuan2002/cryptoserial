#!/bin/bash -l 

set -e
set -o pipefail

##yuan.chen@duke.edu

die() { echo "$@" ; exit 1; }

if [ $# -lt 9 ]
then
    echo "Usage: $0 <Ref_seqs> <Bam_File> <BD_Brks> <DL_Brks> <CV_Brks> <PD_Brks> <Ins_Size> <Out_Name> <Out_File>"
    die ""
fi

BIN_FOLDER=$(dirname $0)
VCF_MERGE=$BIN_FOLDER/SV_VCFBreakMerge.pl
BREAK_CHECK=$BIN_FOLDER/SV_BreakCheck.pl
BREAK_FILTER=$BIN_FOLDER/SV_CheckFilter.pl
DEPTH_VERIFY=$BIN_FOLDER/SV_DepthVerify.py
FASTA_LENGTH=$BIN_FOLDER/Seq_FasLength.pl

RefSeq=$1
BAMFile=$2
BD_Brks=$3
DL_Brks=$4
CV_Brks=$5
PD_Brks=$6
Ins_Size=$7
OutName=$8
RefSeqLength=$1.len

[ -r $RefSeq ] || die "File: $1 does not appear to be valid"
[ -r $BD_Brks ] || die "File: $2 does not appear to be valid"
[ -r $DL_Brks ] || die "File: $3 does not appear to be valid"
[ -r $CV_Brks ] || die "File: $4 does not appear to be valid"
[ -r $PD_Brks ] || die "File: $5 does not appear to be valid"

[ -x $VCF_MERGE ] || die "$VCF_MERGE is not found or executable"
[ -x $BREAK_CHECK ] || die "$BREAK_CHECK is not found or executable"
[ -x $BREAK_FILTER ] || die "$BREAK_FILTER is not found or executable"
[ -x $DEPTH_VERIFY ] || die "$DEPTH_VERIFY is not found or executable"


ConfigFile=$OutName.fls
LogFile=$OutName.log
BrkAll=$OutName.del.all.brk
BrkChk1=$OutName.del.chk.brk
BrkFlt1=$OutName.del.chk.flt.brk
BrkChk2=$OutName.del.chk2.brk
BrkFinal=$9

TMPDIR=/netscratch/yc136/tmp

[ -d $TMPDIR ] || mkdir $TMPDIR
[ -r $RefSeqLength ] || $FASTA_LENGTH $RefSeq > $RefSeqLength

echo `date "+%Y-%m-%d %H:%M:%S"` > $LogFile

echo -e "BD=$BD_Brks\nDL=$DL_Brks\nCV=$CV_Brks\nPD=$PD_Brks" > $ConfigFile

echo "$VCF_MERGE -i $ConfigFile -t DEL -c $RefSeq > $BrkAll" >> $LogFile
$VCF_MERGE -i $ConfigFile -t DEL -c $RefSeq > $BrkAll

echo "$BREAK_CHECK -i $BrkAll -o $BrkChk1 --ref $RefSeq --tmp $TMPDIR --bam $BAMFile --ins $Ins_Size" >> $LogFile
$BREAK_CHECK -i $BrkAll -o $BrkChk1 --ref $RefSeq --tmp $TMPDIR --bam $BAMFile --ins $Ins_Size

echo "$BREAK_FILTER $BrkChk1 0 > $BrkFlt1" >> $LogFile
$BREAK_FILTER $BrkChk1 0 > $BrkFlt1

echo "$DEPTH_VERIFY $BrkFlt1 $BAMFile $RefSeqLength > $BrkChk2" >> $LogFile
$DEPTH_VERIFY $BrkFlt1 $BAMFile $RefSeqLength > $BrkChk2

echo "$BREAK_FILTER $BrkChk2 0 > $BrkFinal" >> $LogFile
$BREAK_FILTER $BrkChk2 0 > $BrkFinal

echo `date "+%Y-%m-%d %H:%M:%S"` >> $LogFile

#rm $BrkChk1 $BrkFlt1 $BrkChk2
