#!/bin/bash -l 

set -e
set -o pipefail

die() { echo "$@" ; exit 1; }

if [ $# -lt 4 ]
then
    echo "Usage: $0 <Reference> <in1.fq> <in2.fq> <out> [thread:default 1]"
    die ""
fi

[ -r $1 ] || die "File: $1 does not appear to be valid"
[ -r $2 ] || die "File: $2 does not appear to be valid"
[ -r $3 ] || die "File: $3 does not appear to be valid"

BWA=/home/mmrl/yc136/bin/bwa-0.7.10/bwa
SAMTOOLS=/home/mmrl/yc136/bin/samtools/samtools

[ -x $BWA ] || "BWA is not found or executable"
[ -x $SAMTOOLS ] || die "samtools is not found or executable"

OutSam=$4.sam
OutBam=$4.bam
SortedBam=$4.sorted
Thread=$5
: ${Threads:=1}

if [ -r $1.sa ]
then
    $BWA index $1
fi

echo "$BWA mem -M $1 $2 $3 > $OutSam"
$BWA mem -M -t $Thread $1 $2 $3 > $OutSam
echo "$SAMTOOLS view -S -b -u $OutSam > $OutBam"
$SAMTOOLS view -S -b -u $OutSam > $OutBam
echo
$SAMTOOLS sort $OutBam $SortedBam

rm $OutSam $OutBam
