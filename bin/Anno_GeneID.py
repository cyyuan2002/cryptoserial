#!/usr/bin/env python
import sys
import csv
import GFFParser

if(len(sys.argv) < 2):
    sys.stderr.write ("Usage: <GFF_File> <Gene_Ids>\n")
    sys.exit(1)

gfffile = sys.argv[1]
gidfile = sys.argv[2]

gffparser = GFFParser.GFFParser(gfffile)

gids = [line.strip() for line in open(gidfile, 'r')]
for gid in gids:
    geneinfo = gffparser.getGene(gid)
    if(geneinfo):
        region = "%s : %s - %s (%s)" %(geneinfo.chrom, geneinfo.s,geneinfo.e,geneinfo.d)
        print ("%s\t%s\t%s" %(gid, region,geneinfo.name))
    else:
        print ("%s\tNot found" %(gid))
