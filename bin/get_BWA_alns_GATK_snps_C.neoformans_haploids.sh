#!/bin/sh
#______________________________________________________________________________________
# SCRIPT TO ALIGN PAIRED END ILLUMINA READS TO A REFERENCE ASSEMBLY AND CALL SNPs
# INPUT FILES REQUIRED
# $1 --> Reference Assembly in fasta format
# $2 --> Left/1st of Paired Read in fastq format
# $3 --> Right/1st of Paired Read in fastq format
# $4 --> Reference_Read_Prefix
# $5 --> RGID for AddOrReplaceReadGroups.jar
# $6 --> Output folder
# Include the paths for AddOrReplaceReadGroups.jar & GenomeAnalysisTK.jar
#______________________________________________________________________________________

# USAGE FOR THE COMMAND
#______________________________________________________________________________________
if [ $# -le 5 ]
then
	echo "Usage: $0 <reference.assembly.fasta> <pair1.reads.fastq> <pair2.reads.fastq> <aRef_rReads_prefix> <RGID_Name> <Output_Folder>"
	exit 1
fi

if [ $6 ]
then
	cd $6
fi

# create shortcuts to the necessary jar files
#______________________________________________________________________________________
# AddOrReplaceReadGroups.jar
if [ ! -f AddOrReplaceReadGroups.jar ]
then
	ln -s /dscrhome/yc136/bin/picard-tools-1.106/AddOrReplaceReadGroups.jar
fi
   
# GenomeAnalysisTK.jar 2.7-4   
if [ ! -f GenomeAnalysisTK.jar ]
then
	ln -s /dscrhome/yc136/bin/GATK-2.7.4/GenomeAnalysisTK.jar
fi

if [ ! -f CreateSequenceDictionary.jar ]
then
	ln  -s /dscrhome/yc136/bin/picard-tools-1.106/CreateSequenceDictionary.jar
fi


# ALIGNMENT
#______________________________________________________________________________________
# generate alignment coordinates in suffix array (sa) format

if [ ! -f $.sa ]
then
	bwa index $1
fi

if [ ! -f $.fai ]
then
	samtools faidx $1
fi

file="$1"
basefile="${file%.*}"
targetdict="$basefile.dict"

if [ ! -f $targetdict ]
then
	java -jar CreateSequenceDictionary.jar R=$1 O=$targetdict
fi

echo "bwa aln $1 $2 > $2.sai"
bwa aln $1 $2 > $2.sai
echo "bwa aln $1 $3 > $3.sai"
bwa aln $1 $3 > $3.sai

# convert the sa coords to sc/ch coords
echo "bwa sampe $1 $2.sai $3.sai $2 $3 > $4.sam"
bwa sampe $1 $2.sai $3.sai $2 $3 > $4.sam

# convert alignments in sam to bam format
echo "samtools import $1.fai $4.sam $4.bam"
samtools import $1.fai $4.sam $4.bam

# sort the alignment bam
echo "samtools sort $4.bam $4.sorted"
samtools sort $4.bam $4.sorted

# index the sorted alignments
echo "samtools index $4.sorted.bam"
samtools index $4.sorted.bam

# fix malformed bam
echo "java -jar -Xmx8g AddOrReplaceReadGroups.jar I=$4.sorted.bam O=$4.fixed.bam SORT_ORDER=coordinate RGID=$5 RGLB=dnaseq RGPL=illumina RGSM=WGS CREATE_INDEX=True RGPU=unknown VALIDATION_STRINGENCY=SILENT"
java -jar -Xmx8g AddOrReplaceReadGroups.jar I=$4.sorted.bam O=$4.fixed.bam SORT_ORDER=coordinate RGID=$5 RGLB=dnaseq RGPL=illumina RGSM=WGS CREATE_INDEX=True RGPU=unknown VALIDATION_STRINGENCY=SILENT

# remove extraneous alignment files
if [ -f $4.fixed.bam ]
then
	#rm $2.sai $3.sai $4.sam $4.bam $4.sorted.bam
	echo "rm $2.sai $3.sai $4.sam $4.bam"
	rm $2.sai $3.sai $4.sam $4.bam
else
	echo "failed to create $4.fixed.bam"
	exit 1
fi

# SNP CALLING
#______________________________________________________________________________________
# generate indel intervals with gatk's RealignerTargetCreator
echo "java -jar -Xmx8g GenomeAnalysisTK.jar -T RealignerTargetCreator -R $1 -I $4.fixed.bam -o $4.indel.intervals"
java -jar -Xmx8g GenomeAnalysisTK.jar -T RealignerTargetCreator -R $1 -I $4.fixed.bam -o $4.indel.intervals

# generate local re-alignments around indels with gatk's IndelRealigner
echo "java -jar -Xmx8g GenomeAnalysisTK.jar -T IndelRealigner -targetIntervals $4.indel.intervals -o $4.indelRealigned.bam -I $4.fixed.bam -R $1"
java -jar -Xmx8g GenomeAnalysisTK.jar -T IndelRealigner -targetIntervals $4.indel.intervals -o $4.indelRealigned.bam -I $4.fixed.bam -R $1

# call snps with gatk's UnifiedGenotyper on the IndelRealigned bam
echo "java -jar -Xmx8g GenomeAnalysisTK.jar -R $1 -T UnifiedGenotyper -I $4.indelRealigned.bam -dcov 20000 -A AlleleBalance -ploidy 1 -o $4.unfilter.vcf -glm BOTH --output_mode EMIT_ALL_SITES"
java -jar -Xmx8g GenomeAnalysisTK.jar -R $1 -T UnifiedGenotyper -I $4.indelRealigned.bam -dcov 20000 -A AlleleBalance -ploidy 1 -o $4.unfilter.vcf -glm BOTH --output_mode EMIT_ALL_SITES

# filter snps based on ecoli paper haploid filter conditions
echo "java -jar GenomeAnalysisTK.jar -T VariantFiltration -R $1 -V $4.unfilter.vcf -o $4.filter.vcf --logging_level ERROR --filterExpression 'ABHom < 0.8 || ((DP - MQ0) < 5) || ((MQ0 / (1.0 * DP)) >= 0.5)' --filterName LowConfidence"
java -jar GenomeAnalysisTK.jar -T VariantFiltration -R $1 -V $4.unfilter.vcf -o $4.filter.vcf --logging_level ERROR --filterExpression "ABHom < 0.8 || ((DP - MQ0) < 5) || ((MQ0 / (1.0 * DP)) >= 0.5)" --filterName LowConfidence

# get only the snp data
grep PASS $4.filter.vcf | awk '$4=="A"||$4=="C"||$4=="G"||$4=="T"' | awk '$5=="A"||$5=="C"||$5=="G"||$5=="T"' > $4.final.body
grep '^#' $4.filter.vcf > $4.final.head
cat $4.final.head $4.final.body > $4.final.vcf
rm $4.final.head $4.final.body