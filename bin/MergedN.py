#!/usr/bin/env python
import sys
import csv

geneinfo = {}
caseinfo = []

for i in xrange(1, len(sys.argv)):
    fileN = sys.argv[i].split('.')
    caseID = 'case_' + fileN[1] + '_' + fileN[2]
    caseinfo.append(caseID)
    fh = open(sys.argv[i], 'r')
    reader = csv.reader(fh, delimiter = '\t')
    reader.next()
    for line in reader:
        kaks = {}
        kaks['dN'] = line[1]
        kaks['dS'] = line[2]
        kaks['NSY'] = line[3]
        kaks['SYN'] = line[4]
        kaks['NON'] = line[5]
        if(line[0] not in geneinfo):
            geneinfo[line[0]] = {}
            geneinfo[line[0]][caseID] = kaks
        else:
            geneinfo[line[0]][caseID] = kaks

sys.stdout.write("#GeneID")
for case in caseinfo:
    sys.stdout.write("\t"+case)
sys.stdout.write("\n")

for gene in geneinfo:
    sys.stdout.write(gene)
    kaksinfo = geneinfo[gene]
    for case in caseinfo:
        if(case not in kaksinfo):
            sys.stdout.write('\t0')
        else:
            sys.stdout.write('\t%s' %(kaksinfo[case]['dN']))
    sys.stdout.write('\n')
