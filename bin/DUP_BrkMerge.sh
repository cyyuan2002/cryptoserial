#!/bin/bash -l

set -e
set -o pipefail
###yuan.chen@duke.edu

die() { echo "$@" ; exit 1; }

if [ $# -lt 6 ]
then
    echo "Usage: $0 <Ref_seqs> <Bam_File> <CV_Brks> <Out_Name> <Out_File> <Ref_Repeatmasker> [Ref_Centromere]"
    die ""
fi

BIN_FOLDER=$(dirname $0)
CNV_DUPMERGE=$BIN_FOLDER/SV_cnvDup_Merge.pl
BREAK_FILTER=$BIN_FOLDER/SV_CheckFilter.pl
DEPTH_VERIFY=$BIN_FOLDER/SV_DepthVerify.py
FASTA_LENGTH=$BIN_FOLDER/Seq_FasLength.pl
REPEAT_MERGE=$BIN_FOLDER/RepeatMerge.pl

RefSeq=$1
BAMFile=$2
CV_Brks=$3
OutName=$4
RefRep=$6
RefCentro=$7
RefSeqLength=$1.len
RefRepMerge=$RefRep.merge

[ -r $RefSeq ] || die "File: $RefSeq does not appear to be valid"
[ -r $BAMFile ] || die "File: $BAMFile does not appear to be valid"
[ -r $CV_Brks ] || die "File: $CV_Brks does not appear to be valid"
[ -r $RefRep ] || die "File: $RefRep does not appear to be valid"

if [ -n $RefCentro ]; then
    [ -r $RefCentro ] || die "File: $RefCentro does not appear to be valid"
fi 

[ -x $CNV_MERGE ] || die "$VCF_MERGE is not found or executable"
[ -x $BREAK_FILTER ] || die "$BREAK_FILTER is not found or executable"
[ -x $DEPTH_VERIFY ] || die "$DEPTH_VERIFY is not found or executable"

ConfigFile=$OutName.fls
LogFile=$OutName.log
BrkAll=$OutName.dup.all.brk
BrkChk1=$OutName.dup.chk.brk
BrkFinal=$5

TMPDIR=/netscratch/yc136/tmp
[ -d $TMPDIR ] || mkdir $TMPDIR

[ -r $RefSeqLength ] || $FASTA_LENGTH $RefSeq > $RefSeqLength
[ -r $RefRepMerge ] || $REPEAT_MERGE $RefRep > $RefRepMerge

echo `date "+%Y-%m-%d %H:%M:%S"` > $LogFile

echo -e "CV=$CV_Brks" > $ConfigFile
if [ ! -n $RefCentro ]; then
    echo "$CNV_DUPMERGE -i $ConfigFile -t DUP -c $RefCentro -r $RefRepMerge > $BrkAll" >> $LogFile
    $CNV_DUPMERGE -i $ConfigFile -t DUP -c $RefCentro -r $RefRepMerge > $BrkAll
else
    echo "$CNV_DUPMERGE -i $ConfigFile -t DUP -r $RefRepMerge > $BrkAll" >> $LogFile
    $CNV_DUPMERGE -i $ConfigFile -t DUP -r $RefRepMerge > $BrkAll
fi

echo "$DEPTH_VERIFY $BrkAll $BAMFile $RefSeqLength > $BrkChk1" >> $LogFile
$DEPTH_VERIFY $BrkAll $BAMFile $RefSeqLength > $BrkChk1

echo "$BREAK_FILTER $BrkChk1 2 > $BrkFinal" >> $LogFile
$BREAK_FILTER $BrkChk1 2 > $BrkFinal

echo `date "+%Y-%m-%d %H:%M:%S"` >> $LogFile
