cd /dscrhome/yc136/Workspace/cryptoserial/data/snps/allSites

##create shell for extracting passed calls
\ls *.VG*.filter.vcf | perl -e '{while(<>){chomp();$file = $_; $file=~s/filter/passed/g;print "perl ~/Workspace/cryptoserial/bin/VCF_GrepPassBroad.pl $_ $file .\n"}}' > passvcf.VG.sh

sh passvcf.VG.sh

##merge vcf files
java -Xmx16g -jar ~/bin/GATK-2.7.4/GenomeAnalysisTK.jar -R ~/Workspace/cryptoserial/data/Reference/IND107/IND107.genome.fasta -T CombineVariants -V:case.31.I case.31.I.VGIV.passed.vcf -V:case.31.R case.31.R.VGIV.passed.vcf -V:case.67.I case.67.I.VGIV.passed.vcf -V:case.67.R case.67.R.VGIV.passed.vcf -o VGIV_Passed_Merged.vcf -genotypeMergeOptions UNIQUIFY

java -Xmx16g -jar ~/bin/GATK-2.7.4/GenomeAnalysisTK.jar -R ~/Workspace/cryptoserial/data/Reference/WM276/WM276.genome.fasta -T CombineVariants -V:case.91.I case.91.I.VGI.passed.vcf -V:case.91.R case.91.R.VGI.passed.vcf -o VGI_Passed_Merged.vcf -genotypeMergeOptions UNIQUIFY

##move files to target folder
cd /dscrhome/yc136/Workspace/cryptoserial/data/snps/matrix

mv ../allSites/VG*_Passed_Merged.* .

##convert vcf to matrix file, remove the sites without any snps
python ~/bin/Python/Format/MergedVCF2Matrix.py VGI_Passed_Merged.vcf VGI_Passed_Merged.matrix 1
python ~/bin/Python/Format/MergedVCF2Matrix.py VGIV_Passed_Merged.vcf VGIV_Passed_Merged.matrix 1

##calculate information sites between relapse and initial strains
echo -e "case91\t4,5" > serial.VGI.groups

python ~/Workspace/cryptoserial/bin/SNPMatrixGroupMerge.py VGI_Passed_Merged.matrix serial.VGI.groups VGI_Passed_Merged.group.matrix

echo -e "case31\t4,5\ncase67\t6,7" > serial.VGIV.groups

python ~/Workspace/cryptoserial/bin/SNPMatrixGroupMerge.py VGIV_Passed_Merged.matrix serial.VGIV.groups VGIV_Passed_Merged.group.matrix

##summarize changed strains in snp locus
python ../../../bin/SNPMatrixSum.py VGI_Passed_Merged.group.matrix > VGI_Passed_Merged.group.matrix.sum

python ../../../bin/SNPMatrixSum.py VGIV_Passed_Merged.group.matrix > VGIV_Passed_Merged.group.matrix.sum

##--Filter VCF file with the matrix file for calculating mutation rate
perl ../../../bin/FilterVCF_Matrix.pl VGI_Passed_Merged.group.matrix VGI_Passed_Merged.vcf > VGI_Passed_Merged.flt.vcf

perl ../../../bin/FilterVCF_Matrix.pl VGIV_Passed_Merged.group.matrix VGIV_Passed_Merged.vcf > VGIV_Passed_Merged.flt.vcf

~/bin/vcfannotator_r2013-10-13/VCF_annotator.pl --gff3 ../../Reference/WM276/WM276.genes.gff3 --genome ../../Reference/WM276/WM276.genome.fasta --vcf VGI_Passed_Merged.flt.vcf > VGI_Passed_Merged.flt.vcf.anno

~/bin/vcfannotator_r2013-10-13/VCF_annotator.pl  --gff3 ../../Reference/IND107/IND107.genes.gff3 --genome ../../Reference/IND107/IND107.genome.fasta --vcf VGIV_Passed_Merged.flt.vcf > VGIV_Passed_Merged.flt.vcf.anno

python ../../../bin/SNPMatrix_Stats.py VGI_Passed_Merged.flt.vcf.anno VGI_Passed_Merged.group.matrix > VGI_Passed_Merged.group.matrix.stats

python ../../../bin/SNPMatrix_Stats.py VGIV_Passed_Merged.flt.vcf.anno VGIV_Passed_Merged.group.matrix > VGIV_Passed_Merged.group.matrix.stats

python ~/bin/Python/Format/NumberSlidingWindow.py VGI_Passed_Merged.group.matrix.sum ../../../data/Reference/WM276/WM276.genome.fasta.len 5000 1000 > VGI_Passed_Merged.group.matrix.sum.5k

python ~/bin/Python/Format/NumberSlidingWindow.py VGIV_Passed_Merged.group.matrix.sum ../../../data/Reference/IND107/IND107.genome.fasta.len 5000 1000 > VGIV_Passed_Merged.group.matrix.sum.5k


python ../../../bin/GFFParser.py ../../Reference/IND107/IND107.genes.gff3 > IND107.genes.gff3.stats

python ../../../bin/GFFParser.py ../../Reference/WM276/WM276.genes.gff3 > WM276.genes.gff3.stats