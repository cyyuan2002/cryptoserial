#!/usr/bin/env python
"""The class is used store SNP annotation from VCF file"""
import re

class LocusInfo(object):
    def __init__(self,chrom,pos,ref,alt,annoinfo):
        self.chrom = chrom
        self.pos = int(pos)
        self.ref = ref
        self.alt = alt
        self.anno_all = annoinfo
        self.gene = ''
        self.iscoding = False
        self.isnsy = False
        self._anaInfo()
        
    def _anaInfo(self):
        info = self.anno_all
        infos = info.split(',')
        if(re.match('CDS', info)):
            self.type = 'CDS'
            self.iscoding = True
            self.gene = infos[1]
            if(infos[-1] == '(NSY)'):
                self.isnsy = True
        elif(re.match('intron', info)):
            self.type = 'intron'
            self.gene = infos[1]
        elif(re.match('p3UTR', info)):
            self.type = 'p3UTR'
            self.gene = infos[1]
        elif(re.match('p5UTR', info)):
            self.type = 'p5UTR'
            self.gene = infos[1]
        elif(re.match('intergenic', info)):
            self.type = 'intergenic'
    
    def isNSY(self):
        return (self.isnsy)
    
    def isCDS(self):
        return (self.iscoding)
    
    def getGene(self):
        if(self.isgene):
            return self.gene
        else:
            return -1
    
    def getType(self):
        return self.type

class ChromAnno(object):
    def __init__(self, chrom):
        self.chrom = chrom
        self.loci = []
        self.lociindex = {}
        self.index = 0

    def addLocus(self, pos, ref, alt, annoinfo):
        pos = int(pos)
        locusinfo = LocusInfo(self.chrom, pos, ref, alt, annoinfo)
        self.loci.append(locusinfo)
        self.lociindex[pos] = self.index
        self.index += 1
    
    def searchLocus(self, pos):
        pos = int(pos)
        if pos in self.lociindex.keys():
            locusinfo = self.loci[self.lociindex[pos]]
            return locusinfo
        else:
            return -1
    
    def numofAnno(self):
        return (self.index)
    
if __name__ == '__main__':
    chrom = ChromAnno("supercont2.1_of_Cryptococcus_neoformans_grubii_H99")
    chrom.addLocus('19883','C','T',"CDS,CNAG_04548,7000009599263693,hypothetical protein,trans_orient:-,loc_in_cds:4812,codon_pos:3,codon:caC-caT,pep:H->H,His-1604-His,(SYN)")
    res = chrom.searchLocus('1983')
    print "Done!"