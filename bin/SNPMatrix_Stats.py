#!/usr/bin/env python
import VCFAnno
import sys
import csv
import re
import copy

class CaseStats(object):
    def __init__(self, name):
        self.name = name
        self.totalcount = 0
        self.codingcount = 0
        self.noncodingcount = 0
        self.syncount = 0
        self.nsycount = 0
        self.p3UTRcount = 0
        self.p5UTRcount = 0
        self.introcount = 0
        self.intercount = 0
    
    def addCount(self,SNPtype):
        self.totalcount += 1
        if SNPtype == 'CDS':
            self.codingcount += 1
        else:
            self.noncodingcount += 1
            if SNPtype == 'intron':
                self.introcount += 1
            elif SNPtype == 'p3UTR':
                self.p3UTRcount += 1
            elif SNPtype == 'p5UTR':
                self.p5UTRcount += 1
            elif SNPtype == 'intergenic':
                self.intercount += 1
    
    def addSyn(self):
        self.syncount += 1
    
    def addNsy(self):
        self.nsycount += 1   

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write ("Usage:%s <VCF_Anno> <SNP_Matrix>\n" %(sys.argv[0]))
        sys.exit(1)
    
    vcffile = sys.argv[1]
    mtxfile = sys.argv[2]
    
    fh_vcf = open(vcffile,'rU')
    chromAnnos = {}
    currChrom = ""
    chromAnno = None
    
    for line in fh_vcf:
        if re.match('^#', line):
            continue;
        stripline = line.strip()
        info = stripline.split('\t')
        if(currChrom != info[0]):
            if(currChrom != ""):
                chromAnnos[currChrom] = copy.copy(chromAnno)
            currChrom = info[0]
            chromAnno = VCFAnno.ChromAnno(info[0])
        chromAnno.addLocus(info[1], info[3], info[4], info[-1])
    
    chromAnnos[currChrom] = copy.copy(chromAnno)
    fh_vcf.close()
    
    fh_mtx = open(mtxfile, 'rU')
    reader = csv.reader(fh_mtx, delimiter="\t")
    CaseInfo = []
    header = reader.next()
    for i in range(2,len(header)):
        CaseInfo.append(CaseStats(header[i]))
    
    currChrom = ""
    currAnno = None
    for line in reader:
        if(line[0] != currChrom):
            currAnno = chromAnnos[line[0]]
            currChrom = line[0]
        locusAnno = currAnno.searchLocus(line[1])
        if(not locusAnno):
            sys.stderr.write ("Error: cannot find annotation for %s:%s" %(line[0],line[1]))
        locusType = locusAnno.getType()
        locusNsy = locusAnno.isNSY()
        locusCDS = locusAnno.isCDS()
        for i in range(2,len(line)):
            if(line[i] == '1'):
                index = i - 2
                CaseInfo[index].addCount(locusType)
                if(locusCDS):
                    if(locusNsy):
                        CaseInfo[index].addNsy()
                    else:
                        CaseInfo[index].addSyn()
    fh_mtx.close()
    
    print ("Case\tTotal\tCoding\tNon-coding\t5UTR\t3UTR\tIntron\tIntergenic\tSYN\tNSY")
    for i in range(len(CaseInfo)):
        statinfo = CaseInfo[i]
        print("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s" \
              %(statinfo.name, statinfo.totalcount, statinfo.codingcount, statinfo.noncodingcount, \
                statinfo.p5UTRcount, statinfo.p3UTRcount, statinfo.introcount, statinfo.intercount, \
                statinfo.syncount, statinfo.nsycount))
