#!/usr/bin/env python
"""This script is used to convert the region number data to sites data.
Input data sample1:
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	3384	12723
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	19964	20492
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	31047	31111
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	36315	36364
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	38319	38354
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	44467	44515
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	44600	44637

Input data sample2:
supercont2.14	340001	343000	1.91
supercont2.1	339501	350400	2.96

The output file will be like:
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959002	4
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959011	4
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959015	4
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	959018	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	968361	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	969906	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	969980	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	970059	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	970620	3
supercont2.1_of_Cryptococcus_neoformans_grubii_H99	971222	4
"""
import csv
import sys

if len(sys.argv) < 2:
    sys.stderr.write ("Usage:%s <input_regionfile>\n")
    sys.exit(1)

infile = sys.argv[1]
fh_file = open(infile, 'r')
reader = csv.reader(fh_file, delimiter="\t")

for line in reader:
    num = 1
    if len(line) > 3:
        num = int(line[2])
    start = int(line[1])
    end = int(line[2])
    for i in range(start,end+1):
        print ("%s\t%s\t%s" %(line[0], i, num))

fh_file.close()