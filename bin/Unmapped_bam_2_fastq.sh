#!/bin/sh
#______________________________________________________________________________________
# SCRIPT TO EXTRACT UNMAPPED READS FROM BAM AND OUTPUT AS FASTQ FILES FOR FURTHER ANALYSIS
# INPUT FILES REQUIRED
# $1 --> BAM_FILE
# $2 --> OUTPUT_FILE
#______________________________________________________________________________________

if [ $# -ne 2 ]
then
	echo "Usage: $0 <Bam_file> <Output_file>"
	exit 1
fi

##check exists of samtools and bam2fastq
if ! type samtools >/dev/null; then
	echo "Error: cannot find samtools!"
	exit 1
fi

if ! type bam2fastq >/dev/null; then
	echo "Error: cannot find bam2fastq!"
	exit 1
fi

##extract unmapped reads (flag 4) to bam file
samtools view -uf 4 $1 > $2.bam

##convert bam file to fastq
bam2fastq -o $2\#.fastq $2.bam

echo "$1" > $2