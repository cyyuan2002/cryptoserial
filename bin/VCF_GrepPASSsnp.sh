#!/bin/bash -l 

set -e
set -o pipefail
if [ $# -lt 2 ]
then
    echo "Usage: $0 <VCF_in> <VCF_out> <Pattern>"
    exit 1
fi

PATTERN=$3
grep "^#" $1 > $1.head
grep -v "^#" $1 | awk -v patt=${PATTERN} '{if($7==patt) print}' > $1.body
cat $1.head $1.body > $2
rm $1.head $1.body