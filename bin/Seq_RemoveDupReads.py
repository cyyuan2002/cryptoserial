#!/usr/bin/env python

import sys
from Bio import SeqIO
#import HTSeq
#import itertools

fastq1 = sys.argv[1]
fastq2 = sys.argv[2]

handle1 = open(fastq1,'rU')
seqids = []
dupids = []
limitnum = 40000
readcount = 0
for seqrec in SeqIO.parse(handle1, 'fastq'):
    readcount += 1
    seqID = seqrec.id
    seqID = seqID[:-1]
    if seqID not in seqids:
        seqids.append(seqID)
    else:
        dupids.append(seqID)
        print (seqID)
    if readcount == limitnum:
        break

handle1.close()

