#!/usr/bin/env python
import sys
import csv

filen = sys.argv[1]
handle = open(filen,'r')
reader = csv.reader(handle,delimiter='\t')
for line in reader:
    line[0] = line[0].replace("_of_Cryptococcus_neoformans_grubii_H99","")
    line[2] = line[2].replace("_of_Cryptococcus_neoformans_grubii_H99","")
    print "\t".join(line)
handle.close()
sys.exit(0)
