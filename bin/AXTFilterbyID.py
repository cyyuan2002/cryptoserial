#!/usr/bin/env python
import sys

infile = sys.argv[1]
gidfile = sys.argv[2]

gids = []
with open(gidfile, 'rU') as fh_gid:
    for line in fh_gid:
        line = line.strip()
        gids.append(line)

isout = False
with open(infile, 'rU') as fh_in:
    for line in fh_in:
        line = line.strip()
        if(len(line) < 1):
            if(isout):
                print ("")
                isout = False
        if(line in gids):
            isout = True
        if(isout):
            print (line)

