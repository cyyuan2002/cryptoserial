#!/bin/bash -l 

set -e
set -o pipefail

##yuan.chen@duke.edu

die() { echo "$@" ; exit 1; }

if [ $# -lt 8 ]
then
    echo "Usage: $0 <Ref_seqs> <Bam_File> <BD_Brks> <DL_Brks> <Ins_Size> <Out_Name> <Out_File> <Ref_Repeatmasker> [Ref_Centromere]"
    die ""
fi

BIN_FOLDER=$(dirname $0)
VCF_MERGE=$BIN_FOLDER/SV_VCFBreakMerge.pl
BREAK_CHECK=$BIN_FOLDER/SV_BreakCheck.pl
BREAK_FILTER=$BIN_FOLDER/SV_CheckFilter.pl

RefSeq=$1
BAMFile=$2
BD_Brks=$3
DL_Brks=$4
Ins_Size=$5
OutName=$6
OutFile=$7
RefRep=$8
RefCentro=$9
RefSeqLength=$1.len
RefRepMerge=$RefRep.merge

[ -r $RefSeq ] || die "File: $1 does not appear to be valid"
[ -r $BD_Brks ] || die "File: $2 does not appear to be valid"
[ -r $DL_Brks ] || die "File: $3 does not appear to be valid"

[ -x $VCF_MERGE ] || die "$VCF_MERGE is not found or executable"
[ -x $BREAK_CHECK ] || die "$BREAK_CHECK is not found or executable"
[ -x $BREAK_FILTER ] || die "$BREAK_FILTER is not found or executable"

ConfigFile=$OutName.fls
LogFile=$OutName.log
BrkAll=$OutName.ctx.all.brk
BrkChk1=$OutName.ctx.chk.brk

TMPDIR=/netscratch/yc136/tmp

[ -d $TMPDIR ] || mkdir $TMPDIR
[ -r $RefSeqLength ] || $FASTA_LENGTH $RefSeq > $RefSeqLength

echo `date "+%Y-%m-%d %H:%M:%S"` > $LogFile

echo -e "BD=$BD_Brks\nDL=$DL_Brks" > $ConfigFile

if [ ! -n $RefCentro ]; then
    echo "$VCF_MERGE -i $ConfigFile -t CTX -c $RefCentro -r $RefRepMerge > $BrkAll" >> $LogFile
    $VCF_MERGE -i $ConfigFile -t CTX -c $RefCentro -r $RefRepMerge > $BrkAll
else
    echo "$VCF_MERGE -i $ConfigFile -t CTX -r $RefRepMerge > $BrkAll" >> $LogFile
    $VCF_MERGE -i $ConfigFile -t CTX -r $RefRepMerge > $BrkAll
fi

echo "$BREAK_CHECK -i $BrkAll -o $BrkChk1 --ref $RefSeq --tmp $TMPDIR --bam $BAMFile --ins $Ins_Size" >> $LogFile
$BREAK_CHECK -i $BrkAll -o $BrkChk1 --ref $RefSeq --tmp $TMPDIR --bam $BAMFile --ins $Ins_Size

echo "$BREAK_FILTER $BrkChk1 2 > $OutFile" >> $LogFile
$BREAK_FILTER $BrkChk1 0 > $OutFile

echo `date "+%Y-%m-%d %H:%M:%S"` >> $LogFile
