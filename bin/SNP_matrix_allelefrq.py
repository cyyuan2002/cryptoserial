#!/usr/bin/env python
import csv
import sys

Initcols = [3,5,7,9,11,13,15,17,20,22,24,26,28,31,33];
Initcount = 15
Repscount = 17

infile = sys.argv[1]
fhfile = open(infile,'rU')
reader = csv.reader(fhfile, delimiter='\t')
title = reader.next()
print ("\t".join(title))
#InitStrain = list(title[i] for i in Initcols)
#print ("\t".join(InitStrain))
for line in reader:
    colnum = len(line)
    init_allele = {}
    reps_allele = {}
    ndcount = 0
    for i in range(3,colnum):
        if(line[i] != 'nd'):
            if i in Initcols:    
                if(line[i] not in init_allele):
                    init_allele[line[i]] = 1
                else:
                    init_allele[line[i]] += 1
            else:
                if(line[i] not in reps_allele):
                    reps_allele[line[i]] = 1
                else:
                    reps_allele[line[i]] += 1
        else:
            ndcount += 1
    if(ndcount > (Initcount+Repscount) * 0.2 ):
        continue;
    repskeys = reps_allele.keys()
    for key in repskeys:
        if(reps_allele[key] * 2 > Repscount):
            if(key in init_allele):
                if(init_allele[key] <= 5):
                    print ("%s\t%s\t%s" %(key,init_allele[key],reps_allele[key]))
                    print ("\t".join(line))
            else:
                print ("\t".join(line))

fhfile.close()
sys.exit(0)
