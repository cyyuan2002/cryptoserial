#!/bin/bash -l
set -e 
set -o pipefail

###yuan.chen@duke.edu

# Exit with message
die() { echo "$@" ; exit 1; }

if [ $# != 3 ] ; then
    echo "Usage: $0 <sorted.bam> <reference.fasta> <output.bam>"
    exit 1
fi

# Check input files are readable
[ -r $1 ] || die "File: $1 does not appear to be valid"
[ -r $2 ] || die "File: $2 does not appear to be valid"

# Check programs executable
# DSCR folder
PicardTools=/home/mmrl/yc136/bin/picard-tools-1.70

AddGroups="$PicardTools/AddOrReplaceReadGroups.jar"
MarkDup="$PicardTools/MarkDuplicates.jar"
CreateSeqDic="$PicardTools/CreateSequenceDictionary.jar"
ReorderSam="$PicardTools/ReorderSam.jar"

GATK=/home/mmrl/yc136/bin/GATK-2.7.4/GenomeAnalysisTK.jar
#echo $AddGroups

[ -x $AddGroups ] || die "PicardTools/AddOrReplaceReadGroups.jar is not found or executable"
[ -x $MarkDup ] || die "PicardTools/MarkDuplicates.jar is not found or executable"
[ -x $ReorderSam ] || die "PicardTools/CreateSequenceDictionary.jar is not found or executable"
[ -x $GATK ] || die "GenomeAnalysisTK.jar is not found or executable"
type samtools >/dev/null 2>&1 || die "samtools is not found or executable"

BAMFILE=${1%%.bam}
BAM1="$BAMFILE.group.bam"
BAM2="$BAMFILE.group.noDup.bam"
BAM3="$BAMFILE.group.noDup.reordered.bam"
BAM4=$3
TMPDIR=`pwd`/$1.tmp
[ ${2##*.} == "fasta" ] || die "fasta reference does not end in .fasta"
REFDICT=${2//fasta/dict}
REFINDEX="$2.fai"
DUPMETRICS="$1.duplicateMetricsFile.txt"
INDELINTERVALS="$1.IndelRealigner.intervals"
LOGFILE="$1.realign.log"

echo "Preprocessing $1..." >> $LOGFILE
echo `date "+%Y-%m-%d %H:%M:%S"` >> $LOGFILE
echo "java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $AddGroups I=$1 O=$BAM1 SORT_ORDER=coordinate RGID=foo RGLB=dnaseq RGPL=illumina RGSM='WGS' CREATE_INDEX=True RGPU=unknown VALIDATION_STRINGENCY=SILENT" >> $LOGFILE
java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $AddGroups I=$1 O=$BAM1 SORT_ORDER=coordinate RGID=foo RGLB=dnaseq RGPL=illumina RGSM='WGS' CREATE_INDEX=True RGPU=unknown VALIDATION_STRINGENCY=SILENT
echo "java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $MarkDup I=$BAM1 O=$BAM2 M=$DUPMETRICS VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true REMOVE_DUPLICATES=true" >> $LOGFILE
java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $MarkDup I=$BAM1 O=$BAM2 M=$DUPMETRICS VALIDATION_STRINGENCY=SILENT ASSUME_SORTED=true REMOVE_DUPLICATES=true
echo "samtools index $BAM2" >> $LOGFILE
samtools index $BAM2
[ -e $REFINDEX ] || samtools faidx $2
[ -e $REFDICT ] || java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $CreateSeqDic R=$2 O=$REFDICT
echo "java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $ReorderSam I=$BAM2 O=$BAM3 R=$2 VALIDATION_STRINGENCY=LENIENT" >> $LOGFILE
java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $ReorderSam I=$BAM2 O=$BAM3 R=$2 VALIDATION_STRINGENCY=LENIENT
samtools index $BAM3
echo "Indel realigning ..." >> $LOGFILE
echo `date "+%Y-%m-%d %H:%M:%S"` >> $LOGFILE
echo "java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $GATK -T RealignerTargetCreator -I $BAM3 -R $2 -o $INDELINTERVALS -nt 24" >> $LOGFILE
java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $GATK -T RealignerTargetCreator -I $BAM3 -R $2 -o $INDELINTERVALS -nt 24
echo "java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $GATK -T IndelRealigner -S Silent -I $BAM3 -R $2 -targetIntervals $INDELINTERVALS -o $BAM4" >> $LOGFILE
java -Xmx8g -Djava.io.tmpdir=$TMPDIR -jar $GATK -T IndelRealigner -S Silent -I $BAM3 -R $2 -targetIntervals $INDELINTERVALS -o $BAM4

rm $BAM1
rm "$BAMFILE.group.bai"
rm $BAM2
rm "$BAM2.bai"
rm $BAM3
rm "$BAM3.bai"
rm $DUPMETRICS
rm $INDELINTERVALS
rm -rf $TMPDIR

echo "done!!..." >> $LOGFILE
echo `date "+%Y-%m-%d %H:%M:%S"` >> $LOGFILE
 
