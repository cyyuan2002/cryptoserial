#!/usr/bin/env python
import csv
import numpy as np
import sys
import subprocess
from os import path
from io import StringIO

def _CheckFile(filename):
    if(path.isfile(filename)):
        return 1
    else:
        raise IOError ("Can not find file: %s" %(filename))

def _runProcess(exe):
    p = subprocess.Popen(exe, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    while(True):
      retcode = p.poll()
      line = p.stdout.readline()
      #newline = line.decode('utf-8')
      yield line
      if(retcode is not None):
        break

if(len(sys.argv) < 4):
    print ("Usage:%s <SV_File> <Bam_File> <Genome_Length> [Bed_Command]" %(sys.argv[0]))
    sys.exit(1)

SVFile = sys.argv[1]
BamFile = sys.argv[2]
GenomeLength = sys.argv[3]
BedCmd = None

if(len(sys.argv) == 5):
    BedCmd = sys.argv[4]

_CheckFile(SVFile)
_CheckFile(BamFile)
_CheckFile(GenomeLength)
if(BedCmd is None):
    cmd = ["which","bedtools"]
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    res = p.stdout.readlines()
    #bedtools = shutil.which('bedtools')
    if(len(res) > 0):
    	BedCmd = 'bedtools'
    else: 
    	raise IOError ("Can't find 'bedtools'!")
else:
    if(not path.isfile(BedCmd)):
        raise IOError ("Can't find 'bedtools'!")

ChromLength = {}

with open (GenomeLength,'rU') as csvfile:
    reader = csv.reader(csvfile,delimiter='\t')
    for line in reader:
        ChromLength[line[0]] = int(line[1])
        
currchrom = ''
depths = None
ChromDepths = {}

for line in _runProcess([BedCmd,'genomecov','-d','-split','-ibam', BamFile,'-g',GenomeLength]):
    stripedline = line.strip()
    lineinfo = stripedline.split('\t')
    if(currchrom != lineinfo[0]):
        if(currchrom != ''):
            tmparray = depths
            ChromDepths[currchrom] = tmparray
        currchrom = lineinfo[0]
        #print (currchrom + ":" + str(ChromLength[currchrom]))
        depths = np.zeros(ChromLength[currchrom])
    depths[int(lineinfo[1])-1] = int(lineinfo[2])
tmparray = depths
ChromDepths[currchrom] = tmparray

ChromDpMean = {}
for chrom in ChromDepths:
    depths = ChromDepths[chrom]
    mean = np.mean(depths)
    ChromDpMean[chrom] = mean

with open (SVFile,'rU') as csvfile:
    reader = csv.reader(csvfile,delimiter = '\t')
    for line in reader:
        chrom = line[0]
        start = int(line[1])-1
        end = int(line[3])-1
        SVtype = line[4]
        chrDp = ChromDepths[chrom]
        SVDp = chrDp[start:end]
        SVDpmean = np.mean(SVDp)
        if(SVtype == 'DEL'):
            if(SVDpmean < ChromDpMean[chrom]/5):
                line.append('pass')
            else:
                line.append('.')
        elif(SVtype == 'DUP'):
            if(SVDpmean > ChromDpMean[chrom]*1.8):
                line.append('pass')
                rate = SVDpmean / ChromDpMean[chrom]
                line.append(str(format(rate,'.2f')))
            else:
                line.append('.')
                
        print ('\t'.join(line))

sys.exit(0)
