#!/usr/bin/env perl
use strict;

my ($VCF_File,$Min_Length,$SV_Type)=@ARGV;
die "Useage: <VCF_File> <Min_Length> <SV_Type>" if(@ARGV < 3);

open(my $fh_vcf,"$VCF_File") || die "Can't find file: $VCF_File\n";
while(<$fh_vcf>){
    chomp();
    if(/^#/){
        print "$_\n";
        next;
    }
    chomp();
    my @lines=split(/\t/,$_);
    my @infos=split(/;/,$lines[7]);
    my $endsite=0;
    for my $info(@infos){
        if($info=~/END=(\d+)/){
            $endsite=$1;
            last;
        }
    }
    if($endsite-$lines[1] >= $Min_Length){
        if(/SVTYPE=$SV_Type/){
            print "$_\n";
        }
    }
}
close $fh_vcf;

