#shell script for VN mitocondrial SNP analysis
#change to the VN folder on DSCR
cd /dscrhome/yc136/Workspace/cryptoserial/result/Mito/allSites/VN

#create shell for extracting passed calls
\ls *.filter.vcf | perl -e '{while(<>){chomp();$file = $_; $file=~s/filter/passed/g;print "sh ~/Workspace/cryptoserial/bin/VCF_GrepPASSsnp.sh $_ $file PASS\n"}}' > passvcf.sh
sh passvcf.sh

##GATK merge the SNP calls for 35 strains
java -Xmx8g -jar ~/bin/GATK-2.7.4/GenomeAnalysisTK.jar -R ~/Workspace/cryptoserial/data/Reference/H99/H99.mito.dna.fa -T CombineVariants -V:case.9.I case.9.I.passed.vcf -V:case.9.R1 case.9.R1.passed.vcf -V:case.90.I case.90.I.passed.vcf -V:case.90.R1 case.90.R1.passed.vcf -V:case.99.I case.99.I.passed.vcf -V:case.99.R1 case.99.R1.passed.vcf -V:case.99.R2 case.99.R2.passed.vcf -V:case.8.I case.8.I.passed.vcf -V:case.8.R1 case.8.R1.passed.vcf -V:case.87.I case.87.I.passed.vcf -V:case.87.R1 case.87.R1.passed.vcf -V:case.82.I case.82.I.passed.vcf -V:case.82.R1 case.82.R1.passed.vcf -V:case.81.I case.81.I.passed.vcf -V:case.81.R1 case.81.R1.passed.vcf -V:case.7.I case.7.I.passed.vcf -V:case.7.R1 case.7.R1.passed.vcf -V:case.77.I case.77.I.passed.vcf -V:case.77.R1 case.77.R1.passed.vcf -V:case.77.R2 case.77.R2.passed.vcf -V:case.76.I case.76.I.passed.vcf -V:case.76.R1 case.76.R1.passed.vcf -V:case.5.I case.5.I.passed.vcf -V:case.5.R1 case.5.R1.passed.vcf -V:case.45.I case.45.I.passed.vcf -V:case.45.R1 case.45.R1.passed.vcf -V:case.22.I case.22.I.passed.vcf -V:case.22.R1 case.22.R1.passed.vcf -V:case.1.I case.1.I.passed.vcf -V:case.1.R1 case.1.R1.passed.vcf -V:case.15.I case.15.I.passed.vcf -V:case.15.R1 case.15.R1.passed.vcf -V:case.15.R2 case.15.R2.passed.vcf -V:case.14.I case.14.I.passed.vcf -V:case.14.R1 case.14.R1.passed.vcf -o VN_Mito_Passed_Merged.35.vcf -genotypeMergeOptions UNIQUIFY

##convert merged vcf to matrix
python ~/bin/Python/Format/MergedVCF2Matrix.py VN_Mito_Passed_Merged.35.vcf VN_Mito_Passed_Merged.35.matrix 1

java -Xmx8g -jar ~/bin/GATK-2.7.4/GenomeAnalysisTK.jar -R ~/Workspace/cryptoserial/data/Reference/H99/H99.mito.dna.fa -T CombineVariants -V:case.9.I case.9.I.passed.vcf -V:case.9.R1 case.9.R1.passed.vcf -V:case.90.I case.90.I.passed.vcf -V:case.90.R1 case.90.R1.passed.vcf -V:case.8.I case.8.I.passed.vcf -V:case.8.R1 case.8.R1.passed.vcf -V:case.87.I case.87.I.passed.vcf -V:case.87.R1 case.87.R1.passed.vcf -V:case.82.I case.82.I.passed.vcf -V:case.82.R1 case.82.R1.passed.vcf -V:case.81.I case.81.I.passed.vcf -V:case.81.R1 case.81.R1.passed.vcf -V:case.7.I case.7.I.passed.vcf -V:case.7.R1 case.7.R1.passed.vcf -V:case.77.I case.77.I.passed.vcf -V:case.77.R1 case.77.R1.passed.vcf -V:case.77.R2 case.77.R2.passed.vcf -V:case.76.I case.76.I.passed.vcf -V:case.76.R1 case.76.R1.passed.vcf -V:case.5.I case.5.I.passed.vcf -V:case.5.R1 case.5.R1.passed.vcf -V:case.45.I case.45.I.passed.vcf -V:case.45.R1 case.45.R1.passed.vcf -V:case.22.I case.22.I.passed.vcf -V:case.22.R1 case.22.R1.passed.vcf -V:case.1.I case.1.I.passed.vcf -V:case.1.R1 case.1.R1.passed.vcf -V:case.15.I case.15.I.passed.vcf -V:case.15.R1 case.15.R1.passed.vcf -V:case.15.R2 case.15.R2.passed.vcf -V:case.14.I case.14.I.passed.vcf -V:case.14.R1 case.14.R1.passed.vcf -o VN_Mito_Passed_Merged.32.vcf -genotypeMergeOptions UNIQUIFY

python ~/bin/Python/Format/MergedVCF2Matrix.py VN_Mito_Passed_Merged.32.vcf VN_Mito_Passed_Merged.32.matrix 1

##move the result to matrix folder
mv VN_Mito_Passed_Merged.* ../../matrix

##change working folder
cd /dscrhome/yc136/Workspace/cryptoserial/result/Mito/matrix

##create 32 group file "serial.32.groups" manually
#case.1	4,5
#case.14	6,7
#case.15a	8,9
#case.15b	8,10
#case.22	11,12
#case.45	13,14
#case.5	15,16
#case.7	17,18
#case.76	19,20
#case.77a	21,22
#case.77b	21,23
#case.8	24,25
#case.81	26,27
#case.82	28,29
#case.87	30,31
#case.9	32,33
#case.90	34,35

python ~/Workspace/cryptoserial/bin/SNPMatrixGroupMerge.py VN_Mito_Passed_Merged.32.matrix serial.32.groups VN_Mito_Passed_Merged.32.group.matrix
