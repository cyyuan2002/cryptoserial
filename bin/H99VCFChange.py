#!/usr/bin/env python
import sys

filen = sys.argv[1]
handle = open(filen,'r')
for line in handle:
    line = line.strip()
    if line[0]=="#":
        print line
    else:
        splited = line.split('\t')
        splited[0] = splited[0].replace("_of_Cryptococcus_neoformans_grubii_H99","")
        print "\t".join(splited)
handle.close()
sys.exit(0)
