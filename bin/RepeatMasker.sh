#!/bin/bash -l 

set -e 
set -o pipefail

##yuan.chen@duke.edu

die() { echo "$@" ; exit 1; }

# Exit with message
if [ $# -lt 2 ]
then
    echo "Usage: $0 <Genome> <RepbaseLib> <Threads>"
    die ""
fi

#check input file
[ -r $1 ] || die "File: $1 does not appear to be valid"
[ -r $2 ] || die "File: $2 does not appear to be valid"

Genome=$1
Repbase=$2
Threads=$3
: ${Threads:=1}

BINFOLDER=/home/mmrl/yc136/bin/
RepMakserFolder=$BINFOLDER/RepeatMasker
RepScoutFolder=$BINFOLDER/RepeatScout
RepeatMasker=$RepMakserFolder/RepeatMasker
RepeatScout=$RepScoutFolder/RepeatScout
BuildTable=$RepScoutFolder/build_lmer_table
FilterStg1=$RepScoutFolder/filter-stage-1.prl
FilterStg2=$RepScoutFolder/filter-stage-2.prl

[ -x $RepeatMakser ] || die "RepeatMasker is not found or executable"
[ -x $RepeatScout ] || die "RepeatScout/RepeatScout is not found or executable"
[ -x $BuildTable ] || die "RepeatScout/BuildTable is not found or executable"
[ -x $FilterStg1 ] || die "RepeatScout/filter-stage-1.prl is not found or executable"
[ -x $FilterStg2 ] || die "RepeatScout/filter-stage-2.prl is not found or executable"

command -v nseg >/dev/null 2>&1 || die "nseg is not found, which is required by RepeatScout"
command -v trf >/dev/null 2>&1 || die "trf is not found, which is required by RepeatScout"

REPFREQ="$1.freq"
REPOUT="$1.out"
REPLIB1="$1.replib.1.fasta"
REPLIB2="$1.replib.2.fasta"
REPLIB3="$1.replib.3.fasta"
REPLIBFINAL="$1.replib.fasta"
LOGFILE="$1.repmsk.log"

KmerLength=13
RepCut=10

echo "Processing $1..." > $LOGFILE
echo `date "+%Y-%m-%d %H:%M:%S"` >> $LOGFILE
echo "$BuildTable -l $KmerLength -sequence $1 -freq $REPFREQ" >> $LOGFILE
$BuildTable -l $KmerLength -sequence $Genome -freq $REPFREQ
echo "$RepeatScout -l $KmerLength -sequence $1 -freq $REPFREQ -output $REPLIB1" >> $LOGFILE
$RepeatScout -l $KmerLength -sequence $Genome -freq $REPFREQ -output $REPLIB1
$FilterStg1 $REPLIB1 > $REPLIB2
echo "$RepeatMasker -pa $Threads -lib $REPLIB2 $Genome" >> $LOGFILE
#$RepeatMasker -pa $Threads -s -lib $REPLIB2 -nolow -norna -no_is $Genome
$RepeatMasker -pa $Threads -s -lib $REPLIB2 $Genome
cat $REPLIB2 | $FilterStg2 --cat=$REPOUT --thresh=$RepCut > $REPLIB3
cat $Repbase $REPLIB3 > $REPLIBFINAL
echo "$RepeatMasker -pa $Threads -s -lib $REPLIBFINAL -xsmall -gff $Genome" >> $LOGFILE
$RepeatMasker -pa $Threads -s -lib $REPLIBFINAL -xsmall -gff $Genome

rm $REPFREQ $REPLIB1 $REPLIB2 $REPLIB3

echo "done!!..." >> $LOGFILE
echo `date "+%Y-%m-%d %H:%M:%S"` >> $LOGFILE
