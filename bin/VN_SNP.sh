cd /dscrhome/yc136/Workspace/cryptoserial/data/snps/allSites

##create shell for extracting passed calls
\ls *.filter.vcf | perl -e '{while(<>){chomp();$file = $_; $file=~s/filter/passed/g;print "perl ~/Workspace/cryptoserial/bin/VCF_GrepPassBroad.pl $_ $file .\n"}}' > passvcf.sh

sh passvcf.sh

##merge vcf files
java -Xmx16g -jar ~/bin/GATK-2.7.4/GenomeAnalysisTK.jar -R ~/Workspace/cryptoserial/data/Reference/H99/H99.genome.fasta -T CombineVariants -V:case.9.I case.9.I.passed.vcf -V:case.9.R1 case.9.R1.passed.vcf -V:case.90.I case.90.I.passed.vcf -V:case.90.R1 case.90.R1.passed.vcf -V:case.99.I case.99.I.passed.vcf -V:case.99.R1 case.99.R1.passed.vcf -V:case.99.R2 case.99.R2.passed.vcf -V:case.8.I case.8.I.passed.vcf -V:case.8.R1 case.8.R1.passed.vcf -V:case.87.I case.87.I.passed.vcf -V:case.87.R1 case.87.R1.passed.vcf -V:case.82.I case.82.I.passed.vcf -V:case.82.R1 case.82.R1.passed.vcf -V:case.81.I case.81.I.passed.vcf -V:case.81.R1 case.81.R1.passed.vcf -V:case.7.I case.7.I.passed.vcf -V:case.7.R1 case.7.R1.passed.vcf -V:case.77.I case.77.I.passed.vcf -V:case.77.R1 case.77.R1.passed.vcf -V:case.77.R2 case.77.R2.passed.vcf -V:case.76.I case.76.I.passed.vcf -V:case.76.R1 case.76.R1.passed.vcf -V:case.5.I case.5.I.passed.vcf -V:case.5.R1 case.5.R1.passed.vcf -V:case.45.I case.45.I.passed.vcf -V:case.45.R1 case.45.R1.passed.vcf -V:case.22.I case.22.I.passed.vcf -V:case.22.R1 case.22.R1.passed.vcf -V:case.1.I case.1.I.passed.vcf -V:case.1.R1 case.1.R1.passed.vcf -V:case.15.I case.15.I.passed.vcf -V:case.15.R1 case.15.R1.passed.vcf -V:case.15.R2 case.15.R2.passed.vcf -V:case.14.I case.14.I.passed.vcf -V:case.14.R1 case.14.R1.passed.vcf -o VN_Passed_Merged.35.vcf -genotypeMergeOptions UNIQUIFY

java -Xmx16g -jar ~/bin/GATK-2.7.4/GenomeAnalysisTK.jar -R ~/Workspace/cryptoserial/data/Reference/H99/H99.genome.fasta -T CombineVariants -V:case.9.I case.9.I.passed.vcf -V:case.9.R1 case.9.R1.passed.vcf -V:case.90.I case.90.I.passed.vcf -V:case.90.R1 case.90.R1.passed.vcf -V:case.8.I case.8.I.passed.vcf -V:case.8.R1 case.8.R1.passed.vcf -V:case.87.I case.87.I.passed.vcf -V:case.87.R1 case.87.R1.passed.vcf -V:case.82.I case.82.I.passed.vcf -V:case.82.R1 case.82.R1.passed.vcf -V:case.81.I case.81.I.passed.vcf -V:case.81.R1 case.81.R1.passed.vcf -V:case.7.I case.7.I.passed.vcf -V:case.7.R1 case.7.R1.passed.vcf -V:case.77.I case.77.I.passed.vcf -V:case.77.R1 case.77.R1.passed.vcf -V:case.77.R2 case.77.R2.passed.vcf -V:case.76.I case.76.I.passed.vcf -V:case.76.R1 case.76.R1.passed.vcf -V:case.5.I case.5.I.passed.vcf -V:case.5.R1 case.5.R1.passed.vcf -V:case.45.I case.45.I.passed.vcf -V:case.45.R1 case.45.R1.passed.vcf -V:case.22.I case.22.I.passed.vcf -V:case.22.R1 case.22.R1.passed.vcf -V:case.1.I case.1.I.passed.vcf -V:case.1.R1 case.1.R1.passed.vcf -V:case.15.I case.15.I.passed.vcf -V:case.15.R1 case.15.R1.passed.vcf -V:case.15.R2 case.15.R2.passed.vcf -V:case.14.I case.14.I.passed.vcf -V:case.14.R1 case.14.R1.passed.vcf -o VN_Passed_Merged.32.vcf -genotypeMergeOptions UNIQUIFY

##move files to target folder
cd /dscrhome/yc136/Workspace/cryptoserial/data/snps/matrix

mv ../allSites/VN_Passed_Merged.* .

##convert vcf to matrix file, remove the sites without any snps
python ~/bin/Python/Format/MergedVCF2Matrix.py VN_Passed_Merged.35.vcf VN_Passed_Merged.35.matrix 1
python ~/bin/Python/Format/MergedVCF2Matrix.py VN_Passed_Merged.32.vcf VN_Passed_Merged.32.matrix 1

##calculate information sites between relapse and initial strains
##serial.REP1.groups
#case.1	4,5
#case.14	6,7
#case.15	8,9
#case.22	11,12
#case.45	13,14
#case.5	15,16
#case.7	17,18
#case.76	19,20
#case.77	21,22
#case.8	24,25
#case.81	26,27
#case.82	28,29
#case.87	30,31
#case.9	32,33
#case.90	34,35

python ~/Workspace/cryptoserial/bin/SNPMatrixGroupMerge.py VN_Passed_Merged.32.matrix serial.REP1.groups VN_Passed_Merged.REP1.group.matrix

##serial.REP2.groups
#case.15	9,10
#case.77	22,23
#case.99	37,38

python ~/Workspace/cryptoserial/bin/SNPMatrixGroupMerge.py VN_Passed_Merged.35.matrix serial.REP2.groups VN_Passed_Merged.REP2.group.matrix

##--Filter VCF file with the matrix file for calculating mutation rate
perl ../../../bin/FilterVCF_Matrix.pl VN_Passed_Merged.REP1.group.matrix VN_Passed_Merged.35.vcf > VN_Passed_Merged.35.REP1.vcf

perl ../../../bin/FilterVCF_Matrix.pl VN_Passed_Merged.REP2.group.matrix VN_Passed_Merged.35.vcf > VN_Passed_Merged.35.REP2.vcf

# Annotate vcf with gff information
~/bin/vcfannotator_r2013-10-13/VCF_annotator.pl  --gff3 ../../Reference/H99/H99.genes.gff3 --genome ../../Reference/H99/H99.genome.fasta --vcf VN_Passed_Merged.35.REP1.vcf > VN_Passed_Merged.35.REP1.vcf.anno

~/bin/vcfannotator_r2013-10-13/VCF_annotator.pl  --gff3 ../../Reference/H99/H99.genes.gff3 --genome ../../Reference/H99/H99.genome.fasta --vcf VN_Passed_Merged.35.REP2.vcf > VN_Passed_Merged.35.REP2.vcf.anno

python ../../../bin/SNPMatrix_Stats.py VN_Passed_Merged.35.REP1.vcf.anno VN_Passed_Merged.REP1.group.matrix > VN_Passed_Merged.REP1.group.matrix.stats

python ../../../bin/SNPMatrix_Stats.py VN_Passed_Merged.35.REP2.vcf.anno VN_Passed_Merged.REP2.group.matrix > VN_Passed_Merged.REP2.group.matrix.stats

##summarize changed strains in snp locus
python ../../../bin/SNPMatrixSum.py VN_Passed_Merged.REP1.group.matrix > VN_Passed_Merged.REP1.group.matrix.sum

python ../../../bin/SNPMatrixSum.py VN_Passed_Merged.REP2.group.matrix > VN_Passed_Merged.REP2.group.matrix.sum

python ~/bin/Python/Format/NumberSlidingWindow.py VN_Passed_Merged.REP1.group.matrix.sum ../../Reference/H99/H99.genome.fasta.len 5000 1000 > VN_Passed_Merged.REP1.group.matrix.sum.5k

python ~/bin/Python/Format/NumberSlidingWindow.py VN_Passed_Merged.REP2.group.matrix.sum ../../Reference/H99/H99.genome.fasta.len 5000 1000 > VN_Passed_Merged.REP2.group.matrix.sum.5k

##
cut -f8 VN_SNPs_Merged_Selected.brks.anno | sed 's/MID://g' | awk 'BEGIN{RS=","}{print $0}' > VN_SNPs_Merged_Selected.brks.anno.ID
python ../bin/Anno_GeneID.py ../CneoH99.genes.newname.gff3 VN_SNPs_Merged_Selected.brks.anno.ID > VN_SNPs_Merged_Selected.brks.anno.ID.ginfo

##annotation stats of H99
python ../../../bin/GFFParser.py ../../Reference/H99/H99.genes.gff3 > H99.genes.gff3.stats

##
python ~/Dropbox/Python/Anno/GO_Find_enrichment.py --indent VN_SNPs_Merged_Selected.brks.anno.ID ../Refs/H99/H99.genes.ID ../Refs/H99/CNA2_FINAL_CALLGENES_3.GO.tbl --obo ~/Dropbox/Python/Anno/goatools/go-basic.obo --pval 0.05 > VN_SNPs_Merged_Selected.brks.anno.ID.GO
python ~/Dropbox/Python/Anno/GO_Find_enrichment.py --indent case.all.dn.flt.csv.ID ../Refs/H99/H99.genes.ID ../Refs/H99/CNA2_FINAL_CALLGENES_3.GO.tbl --obo ~/Dropbox/Python/Anno/goatools/go-basic.obo --pval 0.05 > case.all.dn.flt.csv.ID.GO
python ../bin/Anno_GeneID.py ../Refs/H99/CneoH99.genes.newname.gff3 VN_SNPs_Merged_Selected.brks.anno.ID > VN_SNPs_Merged_Selected.brks.anno.ID.ginfo