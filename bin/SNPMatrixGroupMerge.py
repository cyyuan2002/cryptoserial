#!/usr/bin/env python

import sys
import csv

if len(sys.argv) < 4:
    sys.stderr.write ("Usage:%s <Input_Matrix> <Group_File> <Output_Matrix>\n" %(sys.argv[0]))
    sys.exit(1)

infile = sys.argv[1]
groupfile = sys.argv[2]
outfile = sys.argv[3]

fh_gpfile = open(groupfile, 'r')
groupinfo = {}
groupids = []

for line in fh_gpfile:
    striped = line.strip()
    info = striped.split('\t')
    cols = info[1].split(',')
    cols = map(int, cols)
    groupids.append(info[0])
    groupinfo[info[0]] = cols
fh_gpfile.close()

fh_out = open(outfile, 'w')

#header for output
fh_out.write("#CHROM\tPOS")
for groupid in groupids:
    fh_out.write("\t%s" %(groupid))
fh_out.write("\n")


fh_mtxfile = open(infile, 'rU')
csv_reader = csv.reader(fh_mtxfile, delimiter="\t")
csv_reader.next()

for line in csv_reader:
    groupres = [] 
    for groupid in groupids:
        groupgenos = []
        groupcols = groupinfo[groupid]
        for i in range(len(groupcols)):
            if(line[groupcols[i]] not in groupgenos and line[groupcols[i]] != '.'):
                groupgenos.append(line[groupcols[i]])
        if(len(groupgenos) > 1):
            groupres.append('1')
        else:
            groupres.append('0')
    groupset = list(set(groupres))
    if(len(groupset) > 1):
        fh_out.write("%s\t%s\t%s\n" %(line[0],line[1],'\t'.join(groupres)))
    elif(len(groupset) == 1 and groupset[0] == '1'):
        fh_out.write("%s\t%s\t%s\n" %(line[0],line[1],'\t'.join(groupres)))

fh_out.close()
fh_mtxfile.close()