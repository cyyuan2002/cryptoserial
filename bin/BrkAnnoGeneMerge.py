#!/usr/bin/env python
import csv
import sys

copynum = {}
caseIDs = []

for i in xrange(1,len(sys.argv)):
    fileN = sys.argv[i]
    fileNinfo = fileN.split('.')
    caseID = fileNinfo[1] + "_" + fileNinfo[2]
    caseIDs.append(caseID)
    with open(fileN, 'rU') as fh:
        reader = csv.reader(fh, delimiter="\t")
        for line in reader:
            if (line[8] == "MID:NA"):
                continue
            IDs = line[8][4:].split(',')
            for j in xrange(len(IDs)):
                if(IDs[j] not in copynum):
                    copynum[IDs[j]] = {}
                copynum[IDs[j]][caseID] = line[5]

sys.stdout.write("GeneID\t" + "\t".join(caseIDs) + "\n")

for geneid in copynum:
    genecopies = copynum[geneid]
    sys.stdout.write(geneid)
    for caseid in caseIDs:
        if (caseid not in genecopies):
            sys.stdout.write("\t1")
        else:
            sys.stdout.write("\t%s" %(genecopies[caseid]))
    sys.stdout.write("\n")
